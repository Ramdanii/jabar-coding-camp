// Soal no 1
var animals = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"] 
animals.sort()
for(var i=0; i<6; i++){
  console.log(animals[i]);
}


//Soal no 2
function processSentence(name, age, address, hobby) {
  console.log("Nama saya " + name + ", umur saya " + age + ", alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby);
}

var name = "Jhon";
var age = 30;
var address = "Jalan Pelesiran";
var hobby = "Gaming";

var fullSentence = processSentence(name,age,address,hobby);
console.log(fullSentence); 


//Soal no 3
function countVowel(str) { 
    const count = str.match(/[aeiou]/gi).length;
    return count;
}

const string = prompt('Enter a string: ');

const result = countVowel(string);

console.log(result);


//Soal no 4
function hitung(angka){
  var hasil = angka * 2 - 2
  return hasil
}
console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(100) ) 