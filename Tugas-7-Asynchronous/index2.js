var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
 const time = 10000

// readBooksPromise(time , books[0])
//    .then( (res) => ) {

//        readBooksPromise(res , books[1])
//         .then( (res) => ) {
//            console.log(res)
//          })

// ATAU LEBIH SINGKAT LAGI

readBooksPromise(time , books[0] ) 
    .then( (res) => ) {
        return readBooksPromise( res, books[1] ) 
    })
    .then( (res) => ) {
        return readBooksPromise( res, books[2] ) 
    })
    .then( (res) => ) {
        return readBooksPromise( res, books[3] ) 
    })

    .catch( (err) => ) {
        console.log("Error  " + err)

    })