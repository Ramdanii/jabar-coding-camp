//Soal no 1
const Luas = (panjang, lebar) => {
	return '${panjang * lebar}'
}

const Keliling = (panjang, lebar) => {
	return '${2 * panjang + lebar}'
}

console.log('{Luas : $(2, 7)}')
console.log('{Keliling : $(2, 7)}')






//Soal no 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName : () => {
      console.log('${firstName} $ {lastName}')
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 






//Soal no 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)






//Soal no 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)






//Soal no 5
const planet = "earth" 
const view = "glass" 
let before = "Lorem ${view}  dolor sit amet, consectetur adipiscing elit, ${planet}"

console.log(before)