//Soal no 1
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok  "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(kalimat){
  var str = kalimat.split(' ')
  
  var newStr = str.filter( function(el){
    return el != ''
  })

  return newStr.length 
}

console.log(jumlah_kata(kalimat_1))



//Soal no 2
function next_date( hari , bulan , tahun){

  var akhirbulan2
  if(bulan == 2){
    if(tahun % 4 == 0){
      
      if(tahun % 100 == 0){

        if(tahun % 400 == 0){
          akhirbulan2 = 29
        }else{
          akhirbulan2 = 28
        }

      }else{
        akhirbulan2 = 29
      }

    }else{
      akhirbulan2 = 28
    }
  }

  var tanggal_akhir_bulan = {
    1 : 31,
    2 : akhirbulan2,
    3 : 31,
    4 : 30,
    5 : 31,
    6 : 30,
    7 : 31,
    8 : 31,
    9 : 30,
    10 : 31,
    11 : 30,
    12 : 31
  }

  if( hari != tanggal_akhir_bulan[bulan]){
    hari = hari + 1
  }else{
    if(bulan != 12){
      hari = 1
      bulan = bulan + 1
    }else{
      hari = 1
      bulan = 1
      tahun = tahun + 1
    }
  }

  var bulan_str = [
    'Januari' , 'Februari' , 'Maret' , 'April' , 'Mei' , 'Juni' , 'Juli' , 'Agustus' , 'September' , 'Oktober',
    'November' , 'Desember' 
  ]

  return hari + " " + bulan_str[bulan - 1] + " " + tahun

}

var tanggal = 29
var bulan = 2
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun )) 