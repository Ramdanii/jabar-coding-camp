// soal 1
var nilai = "Mengikutitest"
var kesatu = ">=85"
var kedua = ">75 dan <85"

if ( nilai == "Mengikutitest" ) {
    console.log("Indeks nilai saya adalah:")
    if(kesatu == ">=85" ) {
        console.log("A")    
    } else if( kedua == ">75 dan <85") {
        console.log("B")
    }
} else {
    console.log("Tidak memiliki nilai")
}

// soal 2
var tanggal = 06;
var bulan = 11;
var tahun = 2002;
switch(bulan) {
  case 1:   { console.log('06 November 2002'); break; }
  case 2:   { console.log('06 November 2002'); break; }
  case 3:   { console.log('06 November 2002'); break; }
  case 4:   { console.log('06 November 2002'); break; }
  case 5:   { console.log('06 November 2002'); break; }
  case 6:   { console.log('06 November 2002'); break; }
  case 7:   { console.log('06 November 2002'); break; }
  case 8:   { console.log('06 November 2002'); break; }
  case 9:   { console.log('06 November 2002'); break; }
  case 10:   { console.log('06 November 2002'); break; }
  case 11:   { console.log('06 November 2002'); break; }
  case 12:   { console.log('06 November 2002'); break; }
  default:  { console.log('Tidak terjadi apa-apa'); }}


// soal 3
function segitiga1(panjang) {
    let hasil = '';
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '# ';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga1(3));